import java.util.ArrayList;
import java.util.List;

public class Receipt implements Serializable {
    private List<ReceiptItem> itemList;
    private double totalCost;
    private String employeeName;
    private int registerNr;
    private int customerNr;

    public Receipt(String employeeName, int registerNr, int customerNr) {
        this.employeeName = employeeName;
        this.registerNr = registerNr;
        this.customerNr = customerNr;
        itemList = new ArrayList<>();
        totalCost = 0;
    }

    public void addItem(ReceiptItem item, int amount) {
        itemList.add(item);
        totalCost += item.getUnitCost() * amount;
    }
    
    public double getTotalCost() {
    	return totalCost;
    }
    
    public String getEmployeeName() {
    	return employeeName;
    }
    
    public int getRegisterNr() {
    	return registerNr;
    }
    
    public int getCustomerNr() {
    	return customerNr;
    }
    
    @Override
    public void serialize(Serializer serializer) {
    	serializer.objectStart("Receipt");
    	serializer.addField("employeeName", employeeName);
    	serializer.addField("registerNr", registerNr);
    	serializer.addField("customerNr", customerNr);
    	serializer.addField("itemList", itemList);
    	serializer.addField("totalCost", totalCost);
    	serializer.objectEnd("Receipt");
    }
}