
public class ReceiptItem implements Serializable {
    private String productName;
    private double unitCost;

    public ReceiptItem(String productName, double unitCost) {
        this.productName = productName;
        this.unitCost = unitCost;
    }

    public double getUnitCost() {
        return unitCost;
    }
    
    public void setProductName(String productName) {
    	this.productName = productName;
    }
    
    public String getProductName() {
    	return productName;
    }

    @Override
    public void serialize(Serializer serializer) {
    	serializer.objectStart("ReceiptItem");
    	serializer.addField("productName", productName);
    	serializer.addField("unitCost", unitCost);
    	serializer.objectEnd("ReceiptItem");
    }
}