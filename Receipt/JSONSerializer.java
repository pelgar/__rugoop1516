import java.io.OutputStream;
import java.util.List;

public class JSONSerializer extends Serializer {
	
	public JSONSerializer(OutputStream out) {
        super(out);
    }
    public JSONSerializer() {
        super();
    }

	@Override
	public void objectStart(String objectName) {
		if (objectName.equals("ReceiptItem")){
			outStream.println("\t\t\t{");
			outStream.println("\t\t\t"+objectName+": {");
		}
		else{
			outStream.println("{");
			outStream.println("\t"+objectName+": {");
		}
	}

	//registerNr and customerNr
	@Override
	public void addField(String fieldName, int i) {
		outStream.println("\t\t"+fieldName+":"+i+",");
	}

	//unitCost
	@Override
	public void addField(String fieldName, double d) {
		outStream.println("\t\t\t\t"+fieldName+":"+d);
	}

	@Override
	public void addField(String fieldName, boolean b) {
		outStream.println(fieldName+":"+b);
	}

	//productName and employeeName
	@Override
	public void addField(String fieldName, String s) {
		if (fieldName.equals("employeeName")){
			outStream.println("\t\t"+fieldName+":"+"\""+s+"\"");
		}
		else{
			outStream.println("\t\t\t\t"+fieldName+":"+"\""+s+"\"");
		}
	}

	@Override
	public void addField(String fieldName, List<? extends Serializable> l) {
		for (Serializable item : l) {
			outStream.println("\t\t"+fieldName+": [");
			item.serialize(this);
			outStream.println("\t\t\t},");
            outStream.println("\t\t]");
		}
	}

	@Override
	public void addField(String fieldName, Serializable object) {
		outStream.println("\t"+fieldName+":"+object+",");
	}
	
	//ending and object
	@Override
	public void objectEnd(String objectName) {
		if (objectName.equals("ReceiptItem")){
			outStream.println("\t\t\t\t}");
		}
		else{
			outStream.println("}");
		}
	}
	
}
