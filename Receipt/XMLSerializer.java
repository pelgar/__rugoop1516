import java.io.OutputStream;
import java.util.List;

public class XMLSerializer extends Serializer {
	
	public XMLSerializer(OutputStream out) {
        super(out);
    }
    public XMLSerializer() {
        super();
    }
	
    @Override
	public void objectStart(String objectName) {
    	if (objectName.equals("Receipt")){
    		outStream.println("<"+objectName+">");
    	}
    	else{
		outStream.println("\t\t"+"<"+objectName+">");
    	}
	}
	
    //productName and employeeName
    @Override
	public void addField(String fieldName, String s) {
    	if (fieldName.equals("employeeName")){
    		outStream.println("\t"+"<"+fieldName+">"+"\""+s+"\""+"</"+fieldName+">");
    	}
    	else {
    		outStream.println("\t\t\t"+"<"+fieldName+">"+"\""+s+"\""+"</"+fieldName+">");
    	}    	
	}
    
    //registerNr and customerNr
    @Override
	public void addField(String fieldName, int i) {
		outStream.println("\t"+"<"+fieldName+">"+i+"</"+fieldName+">");
	}
    
	//unitCost and totalCost
    @Override
	public void addField(String fieldName, double d) {
		outStream.println("\t\t\t"+"<"+fieldName+">"+d+"</"+fieldName+">");
	}
    
    //the itemList
    @Override
	public void addField(String fieldName, List<? extends Serializable> l) {
	    	for (Serializable item : l) {
				outStream.println("\t"+"<"+fieldName+">");
				item.serialize(this);
				outStream.println("\t"+"</"+fieldName+">");
	    	}
	}
    	
    @Override
	public void addField(String fieldName, boolean b) {
		outStream.println("<"+fieldName+">");
		outStream.println(b);
		outStream.println("</"+fieldName+">");
	}
	
    @Override
	public void addField(String fieldName, Serializable object) {
		outStream.println("\t"+"<"+fieldName+">");
		outStream.println("\t"+object);
		outStream.println("\t"+"</"+fieldName+">");
	}
    
    //ending object
	@Override
    public void objectEnd(String objectName) {
		if (objectName.equals("Receipt")){
			outStream.println("</"+objectName+">");
		}
		else{
    		outStream.println("\t\t"+"</"+objectName+">");
		}
    }
}