import java.util.Date;

public class RentData {

	private Book book;
	private Member member;
	private Date startDate;
	private Date endDate;
	
	public RentData(Book book, Member member, Date startDate) {
		this.book = book;
		this.member = member;
		this.startDate = startDate;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
	public Book getBook() {
		return book;
	}
	
	public Member getMember() {
		return member;
	}

}