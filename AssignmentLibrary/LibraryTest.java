import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

public class LibraryTest implements Observer {
	
	static Vector<Member> members = new Vector<Member>();
	static Vector<Book> books = new Vector<Book> ();	
	
	public void update(Observable o, Object arg) {
		System.out.println(o + " changed with: " + arg);	
	}
	
	public static void main(String s[]) {
		
		
		books.add(new Book("Cinderella ", "Disney ", "123ABC"));
		books.add(new Book("Little Red Riding Hood ", "Charles Perrault ", "123ABC"));
		books.add(new Book("Snow White ", "Disney ", "123ABC"));
				
		members.add(new Member("Agnes ", "address ", "123ABC"));
		members.add(new Member("Helena ", "address ", "123ABC"));
		members.add(new Member("Rein ", "address ", "123ABC"));
		
		//Book book = new Book("title ", "author ", "123ABC");
		//Member member = new Member("name ", "address ", "123ABC");
		
		Library observable = new Library(books, members);
		LibraryTest observer = new LibraryTest();
		observable.addObserver(observer);
		//observable.addBook(book);
		//observable.addMember(member);
		new MainFrame();
		
	}
	
}