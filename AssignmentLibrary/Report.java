import java.awt.BorderLayout;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Report extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JTextArea reportLog = new JTextArea(6, 20); 
	
	public Report(Vector<Book> books, Vector<Member> members) {
		JScrollPane scrollingArea = new JScrollPane(reportLog);
		
		
		for (Book b : books) {
			reportLog.append(b.bookLog());
		}
		
		JPanel content = new JPanel();
        content.setLayout(new BorderLayout());
        content.add(scrollingArea, BorderLayout.CENTER);
        
        this.setContentPane(content);
        this.setTitle("Report log");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
	}
	

}
