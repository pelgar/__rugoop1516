
import java.awt.event.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;

import javax.swing.*;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

public class MainFrameObjects extends JPanel implements ActionListener{
	
	private JList bookList,memberList;
	private JTextField currentDate;
	private JTextArea Members,availibeBooks,rentedBooks;
	private JButton button;
	private JScrollPane memberPane, bookPane;
	private AddingFrame addition = new AddingFrame();
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	private Vector<Member> mList;
	private Vector<Book> bList;
	private Calendar c = Calendar.getInstance();
	
	public MainFrameObjects(){
		revalidate();
		repaint();
		currentDate = new JTextField(7);
		currentDate.setText(sdf.format(c.getTime()));
		currentDate.setEditable(false);
		
		makeButton("Report"); 
		makeButton("Rent");
		makeButton("Return");
		makeButton("Save");
		makeButton("Restore");	
		
		makeMemberPane();
		makeBookPane();
		makeBookPane();
		
	    makeButton("new Member");
		makeButton("new Book");
		makeButton("Day ++");
		
		add(currentDate);
	}
	
	public void makeButton(String s){
		button = new JButton(s);
		button.setActionCommand(s);
		button.addActionListener(this);
		add(button);
	}
	
	public void makeMemberPane(){
		mList = LibraryTest.members;
		memberList = new JList(mList); 
		memberList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		memberList.setLayoutOrientation(JList.VERTICAL);
		memberList.setVisibleRowCount(-1);
		
		memberPane = new JScrollPane(memberList);
        memberPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        memberPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);        
        memberPane.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        memberPane.setPreferredSize(new Dimension(120, 130));
        
        add(memberPane);
	}
	
	public void makeBookPane(){
		bList = LibraryTest.books;
		bookList = new JList(bList); 
		bookList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		bookList.setLayoutOrientation(JList.VERTICAL);
		bookList.setVisibleRowCount(-1);	
		
		bookPane = new JScrollPane(bookList);
        bookPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        bookPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);        
        bookPane.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        bookPane.setPreferredSize(new Dimension(120, 130));
        
        add(bookPane);
	}
	
	
	public void increaseDay(){
		c.add(Calendar.DATE, 1); 
		currentDate.setText(sdf.format(c.getTime()));
	}
	
	public void actionPerformed(ActionEvent arg0) {
		try {
			if("Report".equals(arg0.getActionCommand())){
				System.out.println("report");
			}
			if("Rent".equals(arg0.getActionCommand())){
				System.out.println("rent");
			}
			if("Return".equals(arg0.getActionCommand())){
				System.out.println("return");
			}
			if("Save".equals(arg0.getActionCommand())){
				System.out.println("save");
			}
			if("Restore".equals(arg0.getActionCommand())){
				System.out.println("restore");
			}
			if("new Member".equals(arg0.getActionCommand())){
				System.out.println("new member");
				addition.newAddition("member");
			}
			if("new Book".equals(arg0.getActionCommand())){
				System.out.println("new book");
				addition.newAddition("book");
			}
			if("Day ++".equals(arg0.getActionCommand())){
				increaseDay();
			}			
		} finally {}
	}
}
