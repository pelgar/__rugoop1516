
public class Member {

	private String name;
	private String address;
	private String memberID;
	
	public Member(String name, String address, String memberID) {
		this.name = name;
		this.address = address;
		this.memberID = memberID;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	
	public String getMemberID() {
		return memberID;
	}
	
	public String toString(){		
		return name;
	}
	
}
