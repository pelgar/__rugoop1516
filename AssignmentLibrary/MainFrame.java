
import javax.swing.*;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Vector;

public class MainFrame extends JFrame {
	static JFrame Frame;
	
	public MainFrame(){
		Frame = new JFrame();
		Frame.setTitle("Library");
		Frame.setSize(420, 275);
		Frame.setResizable(false);
		Frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		
		
		Dimension frameSize = getSize(); 
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Frame.setLocation(screenSize.width / 2 - frameSize.width / 2, screenSize.height / 2 - frameSize.height / 2);
		
		Frame.add(new MainFrameObjects());
		Frame.setVisible(true);
	}

}