import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;



public class AddingFrame extends JFrame implements ActionListener, MouseListener {
	
	private JFrame name;
	private JButton cancel;
	private JButton add;
	private JTextField inputTitle,inputAuthor,inputBookID;
	private MainFrame hello;
	
	public void newAddition(String s){	
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		
		Dimension frameSize = getSize(); 
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		
		
		name = new JFrame("Please enter new " + s +"'s name:");
		name.setSize(300, 100);
		name.setLayout(new GridBagLayout());
		name.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		name.setResizable(false);
		name.setLocation(screenSize.width / 2 - frameSize.width / 2, screenSize.height / 2 - frameSize.height / 2);
		inputTitle = new JTextField("Member/Title");
		inputTitle.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		inputTitle.setEditable(true);
		inputTitle.addMouseListener(this);
		c.gridx = 0;
		c.gridy = 0;
		name.add(inputTitle, c);
		
		inputAuthor = new JTextField("Address/Author");
		inputAuthor.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		inputAuthor.setEditable(true);
		inputAuthor.addMouseListener(this);
		c.gridx = 1;
		c.gridy = 0;
		name.add(inputAuthor, c);
		
		
		inputBookID = new JTextField("MemberID/BookID");
		inputBookID.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		inputBookID.setEditable(true);
		inputBookID.addMouseListener(this);
		c.gridx = 2;
		c.gridy = 0;
		name.add(inputBookID, c);
		
		add = new JButton("add");
		add.addActionListener(this);
		c.gridx = 0;
		c.gridy = 1;
		name.add(add, c);
		
		cancel = new JButton("cancel");
		cancel.addActionListener(this);
		c.gridx = 1;
		c.gridy = 1;
		name.add(cancel, c);	
		
		name.setVisible(true);
	}
	
	
	public void actionPerformed(ActionEvent arg0) {
		String name1,address,memberID,title,author,id;
		try {
			if (arg0.getSource() == inputTitle){
				inputTitle.setText("");
				System.out.println("hello");
			}
			if (arg0.getSource() == inputAuthor){
				inputAuthor.setText("");
			}
			if (arg0.getSource() == inputBookID){
				inputBookID.setText("");
			}
			
			if ((name.getTitle()).equals("Please enter new book's name:")){
				if(arg0.getSource() == add){
					title = inputTitle.getText();
					author = inputAuthor.getText();
					id = inputBookID.getText();
					LibraryTest.books.add(new Book(title + " ", author + " ", id));
					SwingUtilities.updateComponentTreeUI(MainFrame.Frame);
					name.dispose();
				}	
				if(arg0.getSource() == cancel){
					name.dispose();
				}
			}
			else if ((name.getTitle()).equals("Please enter new member's name:")){
				if(arg0.getSource() == add){
					name1 = inputTitle.getText();
					address = inputAuthor.getText();
					memberID = inputBookID.getText();
					LibraryTest.members.add(new Member(name1 + " ", address + " ", memberID));
					SwingUtilities.updateComponentTreeUI(MainFrame.Frame);
					name.dispose();
				}	
				if(arg0.getSource() == cancel){
					name.dispose();
				}
				
			}
		} finally {}
		
	}


	@Override
	public void mouseClicked(MouseEvent arg0) {
		if (arg0.getSource() == inputTitle){
			inputTitle.setText("");
		}
		if (arg0.getSource() == inputAuthor){
			inputAuthor.setText("");
		}
		if (arg0.getSource() == inputBookID){
			inputBookID.setText("");
		}		
	}


	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}	
}
