import java.util.Observable;
import java.util.Vector;


public class Library extends Observable {
	
	private Vector<Book> bookList;
	private Vector<Member> membersList;
	
	public Library(Vector<Book> bookList, Vector<Member> membersList) {
		this.bookList = bookList;
		this.membersList = membersList;
	}
	
	public Vector<Book> getBookList() {
		return bookList;
	}
	
	public Vector<Member> getMembersList() {
		return membersList;
	}
	
	public void isChanged() {
		setChanged();
		notifyObservers();
	}
	
	// adding book to the list
	public void addBook(Book b) {
		bookList.add(b);
		isChanged();
	}
	
	// adding member to the list
	public void addMember(Member m) {
		membersList.add(m);
		isChanged();
	}
	
	// removing a book from the list
	public void removeBook(Book b) {
		bookList.remove(b);
		isChanged();
	}
	
	// removing a member from the list
	public void removeMember(Member m) {
		membersList.remove(m);
		isChanged();
	}
}