import javax.swing.undo.AbstractUndoableEdit;

public class EditEdge  extends AbstractUndoableEdit {
	
	private GraphEdge addedEdge;
	private boolean hasBeenDone;
	private GraphModel Graph;

	public EditEdge(GraphEdge edge, GraphModel Graph) {
		addedEdge = edge;
		this.Graph = Graph;
		redo();
	}

	public void redo() {
		Graph.edges.add(addedEdge);
		hasBeenDone = true;
	}

	public void undo() {
		Graph.edges.remove(addedEdge);
		hasBeenDone = false;
	}

	public boolean canRedo() {
		return !hasBeenDone;
	}

	public boolean canUndo() {
		return hasBeenDone;
	}
}
