import javax.swing.undo.AbstractUndoableEdit;

public class EditRemoveEdge  extends AbstractUndoableEdit{
	private GraphEdge removedEdge;
	private boolean hasBeenDone;
	private GraphModel Graph;

	public EditRemoveEdge(GraphEdge edge , GraphModel Graph) {
		removedEdge=edge;
		this.Graph = Graph;
		redo();
	}

	public void redo() {
		Graph.edges.remove(removedEdge);
		hasBeenDone = true;
	}

	public void undo() {
		Graph.edges.add(removedEdge);
		hasBeenDone = false;
	}

	public boolean canRedo() {
		return !hasBeenDone;
	}

	public boolean canUndo() {
		return hasBeenDone;
	}
}

