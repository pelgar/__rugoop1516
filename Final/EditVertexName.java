import javax.swing.undo.AbstractUndoableEdit;

public class EditVertexName extends AbstractUndoableEdit {
	private String oldname, newname;
	private GraphVertex editedVertex;
	private boolean hasBeenDone;

	public EditVertexName(GraphVertex vertex, String name) {
		editedVertex = vertex;
		newname = name;
		oldname = editedVertex.getName();
		redo();
	}

	public void redo() {
		editedVertex.setName(newname);
		hasBeenDone = true;
	}

	public void undo() {
		editedVertex.setName(oldname);
		hasBeenDone = false;
	}

	public boolean canRedo() {
		return !hasBeenDone;
	}

	public boolean canUndo() {
		return hasBeenDone;
	}
}
