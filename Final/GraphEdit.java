
import java.io.File;

public class GraphEdit {

	private GraphModel model;
	private int frames=0;

	public static void main(String[] args) {
		GraphEdit editor = new GraphEdit();
		if (args.length > 0) {
			File file = new File(args[0]);
			editor.start(file);
		} 
		else {
			editor.start();
		}
	}

	public void start(File file) {
		model = new GraphModel(file);
		new GraphFrame(model,this);
	}	

	public void start() {
		model = new GraphModel();
		new GraphFrame(model,this);
	}

	public void addFrame() {
		frames ++;
	}

	public void closeFrame(){
		frames--;
		if(frames==0){
			System.exit(0);
		}
	}
}
