import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class AddVertexAction extends AbstractAction {

	private GraphFrame mainFrame;

	public AddVertexAction(GraphFrame frame) {
		super("Edit Vertex");
		this.putValue(SHORT_DESCRIPTION, "Edit a vertex");
		this.putValue(MNEMONIC_KEY, KeyEvent.VK_E);
		this.putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
		this.mainFrame = frame;
	}

	public void actionPerformed(ActionEvent arg0) {
		GraphVertex v = mainFrame.getSc().getSelectedVertex();
		String vS = v.getName();
		String s = (String) JOptionPane.showInputDialog(mainFrame, "New Vertex Name:", "Customized Dialog", JOptionPane.PLAIN_MESSAGE, null, null, vS);
		if ((s != null) && (s.length() > 0)) {
			mainFrame.getModel().changeVertexName(v, s);
			return;
		}
	}
}
