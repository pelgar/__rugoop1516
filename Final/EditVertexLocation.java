import java.awt.Point;
import javax.swing.undo.AbstractUndoableEdit;

public class EditVertexLocation extends AbstractUndoableEdit {
	private boolean hasBeenDone;
	private boolean significant;
	private GraphVertex movedVertex;
	private Point previousPoint, newPoint;
	private GraphModel Graph;

	public EditVertexLocation(GraphVertex v, Point p, boolean b) {
		movedVertex = v;
		previousPoint = v.getLocation();
		newPoint = p;
		significant = b;
		redo();
	}

	public void redo() {
		movedVertex.setLocation(newPoint);
		hasBeenDone = true;
	}

	public void undo() {
		movedVertex.setLocation(previousPoint);
		hasBeenDone = false;
		Graph.changed();
	}

	public boolean isSignificant() {
		return significant;
	}

	public boolean canRedo() {
		return !hasBeenDone;
	}

	public boolean canUndo() {
		return hasBeenDone;
	}
}
