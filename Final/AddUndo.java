
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

public class AddUndo extends AbstractAction {

	private GraphFrame mainFrame;

	public AddUndo(GraphFrame frame) {
		super("Undo");
		this.putValue(SHORT_DESCRIPTION, "Undo action");
		this.putValue(MNEMONIC_KEY, KeyEvent.VK_U);
		this.putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.CTRL_MASK));
		this.mainFrame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		getModel().undo();
	}

	private GraphModel getModel() {
		return this.mainFrame.getModel();
	}
}
