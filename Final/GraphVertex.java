
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Observable;


public class GraphVertex extends Observable {

	private String name;
	private Rectangle rect;

	public GraphVertex() {
		this.name = "New Vertex";
		this.rect = new Rectangle(200, 200, 100, 40);
	}

	public GraphVertex(Rectangle rect) {
		this("New Vertex", rect);
	}

	public GraphVertex(String name, Rectangle rect) {
		this.name = name;
		this.rect = rect;
	}

	public boolean contains(Point p) {
		return rect.x < p.x && rect.x + rect.width > p.x && rect.y < p.y && rect.y + rect.height > p.y;
	}

	private void changed() {
		this.setChanged();
		this.notifyObservers();
	}

	public Dimension getDimension() {
		return this.rect.getSize();
	}

	public void setDimension(Dimension d) {
		this.rect.setSize(d);
		this.changed();
	}

	public Point getLocation() {
		return this.rect.getLocation();
	}

	public void setLocation(Point p) {
		this.rect.setLocation(p);
		this.changed();
	}

	public void setRect(Rectangle rect) {
		this.rect = rect;
		this.changed();
	}

	public Rectangle getRect() {
		return rect;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		this.changed();
	}
}
