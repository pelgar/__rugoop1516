
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;


public class AddRemoveEdge extends AbstractAction {

	private GraphFrame mainFrame;

	public AddRemoveEdge(GraphFrame frame) {
		super("Remove Edge");
		this.putValue(SHORT_DESCRIPTION, "Remove an edge");
		this.putValue(MNEMONIC_KEY, KeyEvent.VK_R);
		this.putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		this.mainFrame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		mainFrame.getSc().setRemoveEdge();
	}
}
