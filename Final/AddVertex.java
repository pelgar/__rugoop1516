import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;


public class AddVertex extends AbstractAction {

	private GraphFrame mainFrame;

	public AddVertex(GraphFrame frame) {
		super("Add Vertex");
		this.putValue(SHORT_DESCRIPTION, "Add a vertex");
		this.putValue(MNEMONIC_KEY, KeyEvent.VK_A);
		this.putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F, ActionEvent.CTRL_MASK));
		this.mainFrame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		GraphVertex vert = new GraphVertex();
		Dimension size = mainFrame.getPanel().getSize();
		Point middle = new Point(size.width / 2 - vert.getDimension().width / 2, size.height / 2 - vert.getDimension().height / 2);
		vert.setLocation(middle);
		getModel().addVertex(vert);
	}

	private GraphModel getModel() {
		return this.mainFrame.getModel();
	}
}
