
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

public class AddSave extends AbstractAction {

	private GraphFrame mainFrame;

	public AddSave(GraphFrame frame) {
		super("Save");
		this.putValue(SHORT_DESCRIPTION, "Save to a file");
		this.putValue(MNEMONIC_KEY, KeyEvent.VK_S);
		this.putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		this.mainFrame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (getModel().getFile() != null) {
			getModel().save(getModel().getFile());
		} 
		else {
			new AddSaveAs(mainFrame).actionPerformed(null);
		}
	}

	private GraphModel getModel() {
		return this.mainFrame.getModel();
	}
}
