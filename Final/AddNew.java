
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

public class AddNew extends AbstractAction {

	private GraphFrame mainFrame;

	public AddNew(GraphFrame frame) {
		super("New");
		this.putValue(SHORT_DESCRIPTION, "New Graph");
		this.putValue(MNEMONIC_KEY, KeyEvent.VK_N);
		this.putValue(ACCELERATOR_KEY,KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		this.mainFrame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		GraphModel model = mainFrame.getModel();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Graph Files", "graph");
		if (model.isChanged()) {
			String[] options = new String[] { "Discard Changes", "Save and Create new Graph", "Cancel" };
			int response = JOptionPane.showOptionDialog(null, "Close without saving?", "Close?", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[2]);
			if (response == 1) {
				if (model.getFile() != null) {
					model.save(model.getFile());
				} else {
					JFileChooser SaveChooser = new JFileChooser(model.getFile());
					SaveChooser.setFileFilter(filter);
					int returnVal = SaveChooser.showSaveDialog(mainFrame);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						model.save(SaveChooser.getSelectedFile());
					}
					if (returnVal == JFileChooser.CANCEL_OPTION) {
						return;
					}
				}
			} else if (response == JOptionPane.CANCEL_OPTION || response == JOptionPane.CLOSED_OPTION) {
				return;
			}
		}
		mainFrame.setModel(new GraphModel());
	}
}
