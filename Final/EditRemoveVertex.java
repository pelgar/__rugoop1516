import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.undo.AbstractUndoableEdit;

public class EditRemoveVertex extends AbstractUndoableEdit {
	private ArrayList<GraphEdge> removedEdges = new ArrayList<GraphEdge>();
	private GraphVertex removedVertex;
	private boolean hasBeenDone;
	private GraphModel Graph;

	public EditRemoveVertex(GraphVertex vertex, GraphModel Graph) {
		removedVertex = vertex;
		this.Graph = Graph;
		redo();
	}

	public void redo() {
		removedEdges.clear();
		Graph.vertices.remove(removedVertex);
		removedVertex.deleteObserver(Graph);
		for (Iterator<GraphEdge> it = Graph.edges.iterator(); it
				.hasNext();) {
			GraphEdge next = it.next();
			if (next.connects(removedVertex)) {
				this.removedEdges.add(next);
				it.remove();
			}
		}
		hasBeenDone = true;
	}

	public void undo() {
		Graph.vertices.add(removedVertex);
		removedVertex.addObserver(Graph);
		Graph.edges.addAll(removedEdges);
		hasBeenDone = false;
	}

	public boolean canRedo() {
		return !hasBeenDone;
	}

	public boolean canUndo() {
		return hasBeenDone;
	}
}
