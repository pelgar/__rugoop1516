
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;

public class AddNewFrame extends AbstractAction {

	private GraphFrame mainFrame;

	public AddNewFrame(GraphFrame frame) {
		super("New Window");
		this.putValue(SHORT_DESCRIPTION, "Open a new Window");
		this.putValue(MNEMONIC_KEY, KeyEvent.VK_W);
		this.mainFrame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		new GraphFrame(mainFrame.getModel(),mainFrame.getEditor());
	}

}
