
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class GraphFrame extends JFrame implements Observer {
	private GraphEdit editor;
	private GraphModel model;
	private GraphPanel panel;
	private SelectionController sC;

	private JMenuBar menuBar;
	private JMenu fileMenu;
	private JMenu editMenu;
	private JMenu viewMenu;

	private JMenuItem menuItemNew;
	private JMenuItem menuItemOpen;
	private JMenuItem menuItemSave;
	private JMenuItem menuItemSaveAs;
	private JMenuItem menuItemExit;

	private JMenuItem menuItemUndo;
	private JMenuItem menuItemRedo;
	private JMenuItem menuItemAddVertex;
	private JMenuItem menuItemRemoveVertex;
	private JMenuItem menuItemEditVertex;
	private JMenuItem menuItemAddEdge;
	private JMenuItem menuItemRemoveEdge;

	private JMenuItem menuItemNewFrame;

	public GraphFrame(GraphModel model, GraphEdit editor) {
		this.editor = editor;
		this.model = model;
		this.model.addObserver(this);
		this.sC = new SelectionController(this);
		this.sC.addObserver(this);
		this.panel = new GraphPanel(this.model, sC);
		editor.addFrame();
		init();
		this.repaint();
	}

	private void init() {

		this.setSize(1000, 700);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		WindowListener exitListener = new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				exit();
			}
		};
		this.addWindowListener(exitListener);
		this.setLocationRelativeTo(null);

		if (model.getFile() != null) {
			this.setTitle("GraphEditor - " + model.getFile().getName());
		} else {
			this.setTitle("GraphEditor - " + "New Graph");
		}

		this.getContentPane().add(panel);

		menuBar = new JMenuBar();

		fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		menuBar.add(fileMenu);

		Action newAction = new AddNew(this);
		menuItemNew = new JMenuItem(newAction);
		fileMenu.add(menuItemNew);

		Action openAction = new AddOpen(this);
		menuItemOpen = new JMenuItem(openAction);
		fileMenu.add(menuItemOpen);

		Action saveAction = new AddSave(this);
		menuItemSave = new JMenuItem(saveAction);
		fileMenu.add(menuItemSave);

		Action saveAsAction = new AddSaveAs(this);
		menuItemSaveAs = new JMenuItem(saveAsAction);
		fileMenu.add(menuItemSaveAs);

		Action exitAction = new AddExit(this);
		menuItemExit = new JMenuItem(exitAction);
		fileMenu.add(menuItemExit);

		editMenu = new JMenu("Edit");
		editMenu.setMnemonic(KeyEvent.VK_E);
		menuBar.add(editMenu);

		menuItemUndo = new JMenuItem(new AddUndo(this));
		editMenu.add(menuItemUndo);
		menuItemUndo.setEnabled(false);

		menuItemRedo = new JMenuItem(new AddRedo(this));
		editMenu.add(menuItemRedo);
		menuItemRedo.setEnabled(false);

		editMenu.addSeparator();

		menuItemAddVertex = new JMenuItem(new AddVertex(this));
		editMenu.add(menuItemAddVertex);

		menuItemRemoveVertex = new JMenuItem(new AddRemoveVertex(this));
		editMenu.add(menuItemRemoveVertex);
		menuItemRemoveVertex.setEnabled(false);

		menuItemEditVertex = new JMenuItem(new AddVertexAction(this));
		editMenu.add(menuItemEditVertex);
		menuItemEditVertex.setEnabled(false);

		editMenu.addSeparator();

		menuItemAddEdge = new JMenuItem(new AddEdge(this));
		editMenu.add(menuItemAddEdge);
		menuItemAddEdge.setEnabled(false);

		menuItemRemoveEdge = new JMenuItem(new AddRemoveEdge(this));
		editMenu.add(menuItemRemoveEdge);
		menuItemRemoveEdge.setEnabled(false);

		viewMenu = new JMenu("View");
		viewMenu.setMnemonic(KeyEvent.VK_V);
		menuBar.add(viewMenu);

		menuItemNewFrame = new JMenuItem(new AddNewFrame(this));
		viewMenu.add(menuItemNewFrame);

		this.setJMenuBar(menuBar);
		this.setVisible(true);
	}


	public void setModel(GraphModel model) {
		this.model.deleteObserver(this);
		this.model = model;
		this.model.addObserver(this);
		panel.setModel(model);
	}
	
	public GraphModel getModel() {
		return this.model;
	}

	public SelectionController getSc() {
		return this.sC;
	}

	public GraphPanel getPanel() {
		return this.panel;
	}

	public GraphEdit getEditor(){
		return this.editor;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (model.getFile() != null) {
			if (model.isChanged()) {
				this.setTitle("GraphEditor - " + model.getFile().getName() + " *");
			} 
			else {
				this.setTitle("GraphEditor - " + model.getFile().getName());
			}
		} 
		else {
			if (model.isChanged()) {
				this.setTitle("GraphEditor - " + "New Graph *");
			}
			else {
				this.setTitle("GraphEditor - " + "New Graph");
			}
		}
		menuItemUndo.setEnabled(model.canUndo());
		menuItemRedo.setEnabled(model.canRedo());
		if (sC.getSelectedVertex() != null) {
			menuItemAddEdge.setEnabled(true);
			menuItemRemoveEdge.setEnabled(true);
			menuItemRemoveVertex.setEnabled(true);
			menuItemEditVertex.setEnabled(true);
		} 
		else {
			menuItemAddEdge.setEnabled(false);
			menuItemRemoveEdge.setEnabled(false);
			menuItemRemoveVertex.setEnabled(false);
			menuItemEditVertex.setEnabled(false);
		}

	}

	public void exit() {
		if (model.isChanged()) {
			String[] options = new String[] { "Discard Changes","Save and Close", "Cancel" };
			int response = JOptionPane.showOptionDialog(null, "Close without saving?", "Close?", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,	null, options, options[2]);
			if (response == 1) {
				if (model.getFile() != null) {
					model.save(model.getFile());
				} 
				else {
					JFileChooser chooser = new JFileChooser(getModel().getFile());
					FileNameExtensionFilter filter = new FileNameExtensionFilter("Graph Files", "graph");
					chooser.setFileFilter(filter);
					int returnVal = chooser.showSaveDialog(this);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						getModel().save(chooser.getSelectedFile());
					}
					if (returnVal == JFileChooser.CANCEL_OPTION) {
						return;
					}
				}
			} else if (response == JOptionPane.CANCEL_OPTION || response == JOptionPane.CLOSED_OPTION) {
				return;
			}
		}
		editor.closeFrame();
		this.dispose();
	}
}
