
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

public class AddEdge extends AbstractAction {

	private GraphFrame mainFrame;

	public AddEdge(GraphFrame frame) {
		super("Add Edge");
		this.putValue(SHORT_DESCRIPTION, "Add an edge");
		this.putValue(MNEMONIC_KEY, KeyEvent.VK_A);
		this.putValue(ACCELERATOR_KEY,KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.CTRL_MASK));
		this.mainFrame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		mainFrame.getSc().setAddEdge();
	}
}
