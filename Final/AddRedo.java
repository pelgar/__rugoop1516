
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

public class AddRedo extends AbstractAction {

	private GraphFrame mainFrame;

	public AddRedo(GraphFrame frame) {
		super("Redo");
		this.putValue(SHORT_DESCRIPTION, "Redo action");
		this.putValue(MNEMONIC_KEY, KeyEvent.VK_R);
		this.putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Y, ActionEvent.CTRL_MASK));
		this.mainFrame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		getModel().redo();
	}

	private GraphModel getModel() {
		return this.mainFrame.getModel();
	}
}
