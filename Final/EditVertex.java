import javax.swing.undo.AbstractUndoableEdit;

public class EditVertex extends AbstractUndoableEdit {
	private GraphVertex addedVertex;
	private boolean hasBeenDone;
	private GraphModel model;

	public EditVertex(GraphVertex vertex, GraphModel model) {
		addedVertex = vertex;
		this.model = model;
		redo();
	}

	public void redo() {
		model.vertices.add(addedVertex);
		addedVertex.addObserver(this.model);
		hasBeenDone = true;
	}

	public void undo() {
		model.vertices.remove(addedVertex);
		addedVertex.deleteObserver(this.model);
		hasBeenDone = false;
	}

	public boolean canRedo() {
		return !hasBeenDone;
	}

	public boolean canUndo() {
		return hasBeenDone;
	}

}
