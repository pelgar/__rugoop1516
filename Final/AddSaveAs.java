
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;


public class AddSaveAs extends AbstractAction {

	private GraphFrame mainFrame;

	public AddSaveAs(GraphFrame frame) {
		super("Save As");
		this.putValue(SHORT_DESCRIPTION, "Save to a file");
		this.putValue(ACCELERATOR_KEY,KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK| InputEvent.SHIFT_DOWN_MASK));
		this.mainFrame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser chooser = new JFileChooser(getModel().getFile());
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Graph Files", "graph");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showSaveDialog(mainFrame);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			getModel().save(chooser.getSelectedFile());
		}
	}

	private GraphModel getModel() {
		return this.mainFrame.getModel();
	}
}
