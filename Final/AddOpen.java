
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

public class AddOpen extends AbstractAction {
	private GraphFrame mainFrame;

	public AddOpen(GraphFrame frame) {
		super("Open");
		this.putValue(SHORT_DESCRIPTION, "Open a saved file");
		this.putValue(MNEMONIC_KEY, KeyEvent.VK_O);
		this.putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		this.mainFrame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		GraphModel model = mainFrame.getModel();
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Graph Files", "graph");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showOpenDialog(mainFrame);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			if (model.isChanged()) {
				String[] options = new String[] { "Discard Changes", "Save and Open new File", "Cancel" };
				int response = JOptionPane.showOptionDialog(null, "Close without saving?", "Close?", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,	null, options, options[2]);
				if (response == 1) {
					if (model.getFile() != null) {
						model.save(model.getFile());
					} else {
						JFileChooser SaveChooser = new JFileChooser(model.getFile());
						SaveChooser.setFileFilter(filter);
						returnVal = SaveChooser.showSaveDialog(mainFrame);
						if (returnVal == JFileChooser.APPROVE_OPTION) {
							model.save(SaveChooser.getSelectedFile());
						}
						if (returnVal == JFileChooser.CANCEL_OPTION) {
							return;
						}
					}
				} else if (response == JOptionPane.CANCEL_OPTION || response == JOptionPane.CLOSED_OPTION) {
					return;
				}
			}
			GraphModel newModel = new GraphModel(chooser.getSelectedFile());
			mainFrame.setModel(newModel);
		}
	}
}
