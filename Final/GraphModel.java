

import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;


public class GraphModel extends Observable implements Observer {
	private UndoManager undoManager = new UndoManager();

	public ArrayList<GraphVertex> vertices = new ArrayList<GraphVertex>();
	public ArrayList<GraphEdge> edges = new ArrayList<GraphEdge>();

	private File file = null;
	public boolean modelChanged = false;

	public GraphModel() {
	};

	public GraphModel(File file) {
		this.load(file);
		this.file = file;
	}

	public void addVertex(GraphVertex vertex) {
		undoManager.addEdit(new EditVertex(vertex, this));
		this.changed();
	}

	public void removeVertex(GraphVertex vertex) {
		undoManager.addEdit(new EditRemoveVertex(vertex, this));
		this.changed();
	}

	public void changeVertexName(GraphVertex v, String s) {
		undoManager.addEdit(new EditVertexName(v, s));
		this.changed();
	}

	public void changeVertexLocation(GraphVertex v, Point p, boolean b) {
		undoManager.addEdit(new EditVertexLocation(v, p, b));
		this.changed();
	}

	public void addEdge(GraphEdge edge) {
		for (Iterator<GraphEdge> it = edges.iterator(); it.hasNext();) {
			GraphEdge currentEdge = it.next();
			if (currentEdge.getFrom() == edge.getFrom()
					&& currentEdge.getTo() == edge.getTo()) {
				return;
			}
		}
		undoManager.addEdit(new EditEdge(edge, this));
		this.changed();
	}

	public void removeEdge(GraphVertex from, GraphVertex to) {
		GraphEdge removedEdge = null;
		for (GraphEdge e : edges) {
			if (e.getFrom() == from && e.getTo() == to) {
				removedEdge = e;
				break;
			}
		}
		if(removedEdge==null){
			return;
		}
		undoManager.addEdit(new EditRemoveEdge(removedEdge, this));
		this.changed();
	}

	public GraphVertex VertexAtPoint(Point p) {
		for (int i = vertices.size() - 1; i >= 0; i--) {
			if (vertices.get(i).contains(p)) {
				return vertices.get(i);
			}
		}
		return null;
	}

	public void save(File file) {
		if (file != this.file) {
			this.file = file;
		}
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileOutputStream(this.file));
			out.print(vertices.size());
			out.print(" ");
			out.print(edges.size());
			out.println();
			for (Iterator<GraphVertex> itV = vertices.iterator(); itV.hasNext();) {
				GraphVertex vert = itV.next();
				out.print(vert.getLocation().x);
				out.print(" ");
				out.print(vert.getLocation().y);
				out.print(" ");
				out.print(vert.getDimension().width);
				out.print(" ");
				out.print(vert.getDimension().height);
				out.print(" ");
				out.println(vert.getName());
			}
			for (Iterator<GraphEdge> itE = edges.iterator(); itE.hasNext();) {
				GraphEdge edge = itE.next();
				out.print(vertices.indexOf(edge.getFrom()));
				out.print(" ");
				out.print(vertices.indexOf(edge.getTo()));
				out.println();
			}
		} 
		catch (IOException e) {
			System.err.println("Save Error");
		} 
		finally {
			if (out != null) {
				this.modelChanged = false;
				this.setChanged();
				this.notifyObservers();
				out.close();
			}
		}
	}

	public void load(File file) {
		Scanner in = null;
		try {
			in = new Scanner(file);
			int vertAm = in.nextInt();
			int edgeAm = in.nextInt();
			for (int i = 0; i < vertAm; i++) {
				GraphVertex vert = new GraphVertex();
				Point loc = new Point(in.nextInt(), in.nextInt());
				vert.setLocation(loc);
				Dimension size = new Dimension(in.nextInt(), in.nextInt());
				vert.setDimension(size);
				String name = in.nextLine();
				name = name.substring(1);
				vert.setName(name);
				this.vertices.add(vert);
				vert.addObserver(this);
			}
			for (int i = 0; i < edgeAm; i++) {
				GraphVertex from = vertices.get(in.nextInt());
				GraphVertex to = vertices.get(in.nextInt());
				GraphEdge line = new GraphEdge(from, to);
				this.edges.add(line);
			}

		} 
		catch (IOException e) {
			System.err.println("Error in loading");
		}
	}

	public File getFile() {
		return this.file;
	}

	public ArrayList<GraphVertex> getVertices() {
		return vertices;
	}

	public ArrayList<GraphEdge> getEdges() {
		return edges;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		this.setChanged();
		this.notifyObservers();
	}

	public void changed() {
		this.modelChanged = true;
		this.setChanged();
		this.notifyObservers();
	}

	public boolean isChanged() {
		return this.modelChanged;

	}

	public UndoManager getUndoManager() {
		return undoManager;
	}

	public void undo() {
		if (undoManager.canUndo()) {
			undoManager.undo();
			changed();
		}
	}

	public void redo() {
		if (undoManager.canRedo()) {
			undoManager.redo();
			changed();
		}
	}

	public void addEdit(UndoableEdit edit) {
		undoManager.addEdit(edit);
	}

	public boolean canUndo() {
		return undoManager.canUndo();
	}

	public boolean canRedo() {
		return undoManager.canRedo();
	}
}
