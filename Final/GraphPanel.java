
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.font.TextAttribute;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

public class GraphPanel extends JPanel implements Observer {

	final Font f = new Font("SansSerif", Font.BOLD, 12);

	private GraphModel model;
	private SelectionController sC;

	public GraphPanel(GraphModel model, SelectionController sC) {
		this.sC = sC;
		this.addMouseListener(sC);
		this.addMouseMotionListener(sC);
		this.sC.addObserver(this);
		this.model = model;
		this.model.addObserver(this);
	}

	public void setModel(GraphModel model) {
		this.model.deleteObserver(this);
		this.model = model;
		this.model.addObserver(this);
		this.repaint();

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Map<TextAttribute, Object> atts = new HashMap<TextAttribute, Object>();
		Font font = f.deriveFont(atts);

		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,RenderingHints.VALUE_FRACTIONALMETRICS_ON);
		g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		atts.put(TextAttribute.KERNING, TextAttribute.KERNING_ON);

		g2.setFont(font);
		drawEdges(g2);
		drawMouseLine(g2);
		drawVertices(g2);
	}

	private void drawVertices(Graphics2D g2) {
		int [] xP = new int[5];
		int [] yP = new int[5];
		for (GraphVertex vert : model.getVertices()) {
			Dimension d = vert.getDimension();
			Point p = vert.getLocation();
			xP[0] = p.x;
			yP[0] = p.y;			
			xP[1] = p.x + d.width;
			yP[1] = p.y;			
			xP[2] = p.x + d.width;
			yP[2] = p.y + d.height; 			
			xP[3] = p.x;
			yP[3] = p.y + d.height;			
			xP[4] = p.x;			
			yP[4] = p.y;			
			if (vert == sC.getSelectedVertex()) {
				g2.setColor(Color.white);
				g2.fillRect(p.x, p.y, d.width, d.height);
				g2.setColor(Color.black);
				g2.drawPolyline(xP, yP, 5);
			} else {
				g2.setColor(Color.gray);
				g2.fillRect(p.x, p.y, d.width, d.height);
				g2.setColor(Color.black);
				g2.drawPolyline(xP, yP, 5);
			}
			drawCenteredString(vert.getName(), d.width, d.height, p.x, p.y, g2);
		}
	}

	private void drawEdges(Graphics2D g2) {
		for (GraphEdge edge : model.getEdges()) {
			int fromX = edge.getFrom().getLocation().x + (edge.getFrom().getDimension().width / 2);
			int fromY = edge.getFrom().getLocation().y + (edge.getFrom().getDimension().height / 2);
			int toX = edge.getTo().getLocation().x + (edge.getTo().getDimension().width / 2);
			int toY = edge.getTo().getLocation().y + (edge.getTo().getDimension().height / 2);
			g2.drawLine(fromX, fromY, toX, toY);
			drawArrow(g2, fromX, fromY, toX, toY);
		}
	}

	private void drawArrow(Graphics2D g2, int fromX, int fromY, int toX, int toY) {
		int[] xPoints = new int[3];
		int[] yPoints = new int[3];
		int arrow = 20;
		double phi = Math.PI / 6;
		double theta = Math.atan2(toY - fromY, toX - fromX);
		int toXnew = 1 + (int) ((1.0 / 4) * fromX + (3.0 / 4) * toX);
		int toYnew = 1 + (int) ((1.0 / 4) * fromY + (3.0 / 4) * toY);
		xPoints[0] = toXnew;
		yPoints[0] = toYnew;
		xPoints[1] = (int) (toXnew - arrow * Math.cos(theta + phi));
		yPoints[1] = (int) (toYnew - arrow * Math.sin(theta + phi));
		xPoints[2] = (int) (toXnew - arrow * Math.cos(theta - phi));
		yPoints[2] = (int) (toYnew - arrow * Math.sin(theta - phi));
		g2.fillPolygon(xPoints, yPoints, 3);
	}

	private void drawMouseLine(Graphics2D g2) {
		if (sC.isAddEdge() || sC.isRemoveEdge()) {
			if (sC.getSelectedVertex() != null) {
				int fromX = sC.getSelectedVertex().getLocation().x + (sC.getSelectedVertex().getDimension().width / 2);
				int fromY = sC.getSelectedVertex().getLocation().y + (sC.getSelectedVertex().getDimension().height / 2);
				if (sC.isAddEdge()){
					g2.setColor(Color.green);
				}
				if (sC.isRemoveEdge()){
					g2.setColor(Color.red);
				}
				g2.drawLine(fromX, fromY, sC.getMouseLoc().x, sC.getMouseLoc().y);
				drawArrow(g2, fromX, fromY, sC.getMouseLoc().x,sC.getMouseLoc().y);
			}
		}
	}

	private void drawCenteredString(String s, int w, int h, int bx, int by,Graphics2D g2) {
		Rectangle2D r2d = g2.getFontMetrics(f).getStringBounds(s, g2);
		Font fontX = f.deriveFont((float) (f.getSize2D() * ((w - 5) / r2d.getWidth())));
		g2.setFont(fontX);
		r2d = g2.getFontMetrics(fontX).getStringBounds(s, g2);
		if ((h / r2d.getHeight()) < 1) {
			Font fontY = fontX.deriveFont((float) (fontX.getSize2D() * ((h - 2) / r2d.getHeight())));
			g2.setFont(fontY);
		}
		FontMetrics fm = g2.getFontMetrics();
		int x = (w - fm.stringWidth(s)) / 2;
		int y = (fm.getAscent() + (h - (fm.getAscent() + fm.getDescent())) / 2);
		g2.drawString(s, bx + x, by + y);
	}

	@Override
	public void update(Observable o, Object arg) {
		this.repaint();
	}
}
