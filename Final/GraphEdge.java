
public class GraphEdge {
	
	private GraphVertex from;
	private GraphVertex to;
	
	public GraphEdge(GraphVertex from, GraphVertex to) {
		setFrom(from);
		setTo(to);
	}

	public boolean connects(GraphVertex vertex) {
		return from == vertex || to == vertex;
	}

	public GraphVertex getTo() {
		return to;
	}

	public void setTo(GraphVertex to) {
		this.to = to;
	}

	public GraphVertex getFrom() {
		return from;
	}
    
	public void setFrom(GraphVertex from) {
		this.from = from;
	}

}
