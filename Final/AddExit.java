import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

public class AddExit extends AbstractAction {

	private GraphFrame mainFrame;

	public AddExit(GraphFrame frame) {
		super("Exit");
		this.putValue(SHORT_DESCRIPTION, "Exit the program");
		this.putValue(MNEMONIC_KEY, KeyEvent.VK_X);
		this.putValue(ACCELERATOR_KEY,	KeyStroke.getKeyStroke(KeyEvent.VK_F4, ActionEvent.ALT_MASK));
		this.mainFrame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		mainFrame.exit();
	}
}
