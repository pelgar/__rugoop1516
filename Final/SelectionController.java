
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Observable;


public class SelectionController extends Observable implements MouseListener, MouseMotionListener {

	private GraphFrame mainFrame;

	private GraphVertex selectedVertex = null;
	private boolean addEdge;
	private boolean removeEdge;
	private boolean dragging = false;
	private Point mouseLocation;
	private GraphVertex savedVertex;

	private Point savedPoint;
	private Point offsetPoint;

	public SelectionController(GraphFrame frame) {
		this.mainFrame = frame;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			if(selectedVertex!=null){
				new AddVertexAction(mainFrame).actionPerformed(null);
			}			
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (selectedVertex != null && dragging) {
			int x = e.getPoint().x + offsetPoint.x;
			int y = e.getPoint().y + offsetPoint.y;
			Point p = new Point(x, y);
			dragging = false;
			Rectangle rect = selectedVertex.getRect();
			Dimension d = mainFrame.getPanel().getSize();
			if (p.x < 0)
				p.x = 0;
			if (p.y < 0)
				p.y = 0;
			if (p.x > d.width - rect.width)
				p.x = d.width - rect.width;
			if (p.y > d.height - rect.height)
				p.y = d.height - rect.height;
			savedVertex.setLocation(savedPoint);
			mainFrame.getModel().changeVertexLocation(savedVertex, p, true);
		}

	}

	@Override
	public void mousePressed(MouseEvent e) {
		Point mousePoint = e.getPoint();
		GraphModel model = mainFrame.getModel();
		if (addEdge && model.VertexAtPoint(mousePoint) == null) {
			addEdge = false;
		}
		if (removeEdge && model.VertexAtPoint(mousePoint) == null) {
			removeEdge = false;
		}
		if (addEdge && model.VertexAtPoint(mousePoint) != selectedVertex) {
			GraphEdge newEdge = new GraphEdge(selectedVertex, model.VertexAtPoint(mousePoint));
			model.addEdge(newEdge);
			addEdge = false;
		}
		if (removeEdge && model.VertexAtPoint(mousePoint) != selectedVertex) {
			model.removeEdge(selectedVertex, model.VertexAtPoint(mousePoint));
			removeEdge = false;
		}
		selectedVertex = model.VertexAtPoint(mousePoint);
		if (selectedVertex != null) {
			int offsetX = selectedVertex.getLocation().x - e.getPoint().x;
			int offsetY = selectedVertex.getLocation().y - e.getPoint().y;
			offsetPoint = new Point(offsetX, offsetY);
		}
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (selectedVertex != null) {
			int x = e.getPoint().x + offsetPoint.x;
			int y = e.getPoint().y + offsetPoint.y;
			Point p = new Point(x, y);
			if (!dragging) {
				dragging = true;
				savedVertex = selectedVertex;
				savedPoint = p;
			}
			Rectangle rect = selectedVertex.getRect();
			Dimension d = mainFrame.getPanel().getSize();
			if (p.x < 0){
				p.x = 0;
			}
			if (p.y < 0){
				p.y = 0;
			}
			if (p.x > d.width - rect.width){
				p.x = d.width - rect.width;
			}
			if (p.y > d.height - rect.height){
				p.y = d.height - rect.height;
			}
			selectedVertex.setLocation(p);
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if ((addEdge || removeEdge) && selectedVertex != null) {
			mouseLocation = e.getPoint();
			this.setChanged();
			this.notifyObservers();
		}
	}
	
	public GraphVertex getSelectedVertex() {
		return selectedVertex;
	}

	public void setAddEdge() {
		this.addEdge = true;
	}

	public void setRemoveEdge() {
		this.removeEdge = true;
	}

	public boolean isAddEdge() {
		return addEdge;
	}
	
	public boolean isRemoveEdge() {
		return removeEdge;
	}
	
	public Point getMouseLoc() {
		return mouseLocation;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

}
