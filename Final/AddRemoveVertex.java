
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

public class AddRemoveVertex extends AbstractAction {

	private GraphFrame mainFrame;

	public AddRemoveVertex(GraphFrame frame) {
		super("Delete Vertex");
		this.putValue(SHORT_DESCRIPTION, "Remove a vertex");
		this.putValue(MNEMONIC_KEY, KeyEvent.VK_D);
		this.putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.CTRL_MASK));
		this.mainFrame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (mainFrame.getSc().getSelectedVertex() != null) {
			mainFrame.getModel().removeVertex(
					mainFrame.getSc().getSelectedVertex());
		}
	}
}
