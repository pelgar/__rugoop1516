
public class Student extends Person {

	private String studentNumber;

	public Student(String name, String firstName, String address,
			String studentNumber, String phoneNumber) {
		super(name, firstName, address, phoneNumber);
		this.studentNumber = studentNumber;
	}

	public String getStudentNumber() {
		return studentNumber;
	}

	public void setStudentnummer(String studentNumber) {
		this.studentNumber = studentNumber;
	}
	
	public String toString() {
		return super.toString(studentNumber);
	}
	
	public int compare(Person person1, Person person2) {
		return super.compare(person1, person2);
	}
	
}