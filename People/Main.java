
import java.util.Collections;
import java.util.ArrayList;

public class Main {

	public static void main(String[] argv) {

		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Lecturer("Smedinga", "Rein", "Broadway 32", "2345", "0616522783"));
		persons.add(new Student("Styles", "Oliver", "George St 5", "1231231", "0618722984"));
		persons.add(new Student("Horan", "Harry", "Regent St 11", "4564564", "0619277403"));
		persons.add(new Lecturer("Doe", "John", "Main St 153", "6789", "0618022614"));
		persons.add(new Student("Payne", "Jack", "Seven Bridges Way 3", "7897897", "0613988765"));
		persons.add(new Lecturer("White", "Sow", "Fairy Ln 1", "0123", "0618200890"));
		persons.add(new Student("Malik", "Charlie", "York Rd 27", "1011121", "0618911231"));
		
		//3 new professors
		persons.add(new Professor("Dumbledore", "Albus", "Godrics Hollow 4", "0000", "0618243463", "Headmaster"));
		persons.add(new Professor("Snape", "Severus", "Spinners End 13", "1234","0617822345", "Potions Master"));
		persons.add(new Professor("McGonagall", "Minerva", "Caithness 16", "5678", "0617899098", "Transfiguration Master"));

		for (Person person : persons) {
			System.out.println(person);
		}

		System.out.println("\nafter sorting:\n");

		Collections.sort(persons);

		for (Person person : persons) {
			System.out.println(person);
		}

	}

}