
public class Professor extends Lecturer {

	private String title;
	
	public Professor(String name, String firstName, String address,String employeeNumber,
			String phoneNumber, String title) {
		super(name, firstName, address, employeeNumber ,phoneNumber);
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitles(String title) {
		this.title = title;
	}
	
	public String toString() {
		return super.professorToString(title);
	}
	
	public int compare(Person person1, Person person2) {
		return super.compare(person1, person2);
	}
	
}
