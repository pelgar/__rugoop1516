
import java.util.*;

public abstract class Person implements Comparator<Person>, Comparable<Person> {

	private String name;
	private String firstName;
	private String address;
	private String phoneNumber;
	

	public Person(String name, String firstName, String address, String phoneNumber) {
		this.name = name;
		this.firstName = firstName;
		this.address = address;
		this.phoneNumber = phoneNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	

	public String concat(String seperator, String ... string) {
		String concatName = "";
		for (String n : string){
			concatName = concatName + n;
			if (n != string[string.length-1]){
				concatName = concatName + seperator;				
			}
		}
		return concatName;
	}
	
	
	public String toString(String string) {		
		return concat(" ", firstName, name, address, string , phoneNumber);
	}
	
	public int compareTo(Person other) {
		return (this.firstName).compareTo(other.firstName);
	}
	
	@Override
	public int compare(Person person1, Person person2) {
		String name1 = person1.getFirstName();
		String name2 = person2.getFirstName();
		return name1.compareTo(name2);
	}

}