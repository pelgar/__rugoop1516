
public class Lecturer extends Person {
    
    private String employeeNumber;
    
    public Lecturer(String name, String firstName, String address,
                    String employeeNumber, String phoneNumber) {
        super(name, firstName, address, phoneNumber);
        this.employeeNumber = employeeNumber;
    }
    
    public String getEmployeeNumber() {
        return employeeNumber;
    }
    
    public void setEmployeeNumber(String number) {
        employeeNumber = number;
    }
    
    public String toString() {
        return super.toString(employeeNumber);
    }
    
    public String professorToString(String title) {
        return super.toString(employeeNumber + " " + title);
    }
    
    public int compare(Person person1, Person person2) {
        return super.compare(person1, person2);
    }
    
}